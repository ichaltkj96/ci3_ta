<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		// SESSION UNTUK LOGIN
		if($this->session->userdata('level') != 'user'){
			$this->session->set_flashdata('error','Maaf, silahkan login admin terlebih dahulu!');
			redirect('Login');
		}
		// SESSION UNTUK LOGIN

		// LOAD MODEL
		$id_user_hk = $this->session->userdata('level');
		$this->load->model('User_model');
		$this->load->model('Admin_model');
			
		// LOAD HELPER AND LIBRARY
		$this->load->helper(array('url','form'));        
		$this->load->library('form_validation');    
	}
	
	// KUISIONER
	public function index() {
		$config = [
			'upload_path'	=> './assets/images/',
			'allowed_types'	=> 'gif|jpg|png|jpeg',
			'max_size'		=> 999999,
			'max_width' 	=> 999999,
			'max_height'	=> 999999
		];
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload()) { //jika gagal upload 
			$data = array(
				'titile'			=> 'Panel User',
				'head_menu'			=> 'Kuisioner',
				'data_kuisioner'	=> $this->Admin_model->get_data_master_kuisioner(),
			);
			$this->load->view('page/header', $data);
			$this->load->view('user/index', $data);
			$this->load->view('page/footer');
		} else { //jika berhasil upload
			// INSERT HASIL KUISIONER
			$file		= $this->upload->data(); //mengambil data di form
			$pertanyaan	= json_encode($this->input->post('pertanyaan'));
			$jawaban	= json_encode($this->input->post('jawaban'));
			$data_pertanyaan = [
				'id_hasil_kuisioner'	=> $this->Admin_model->get_hasil_kuisioner_id(),
				'id_user'				=> set_value('id_user'),
				'file'					=> $file['file_name'],
				'pertanyaan'			=> $pertanyaan,
				'jawaban'				=> $jawaban,
				'tgl_pengisian'			=> date('Y-m-d')
			];
			$this->Admin_model->insert_hasil_kuisioner($data_pertanyaan); //memasukan data ke database
			$this->email_sender($data_pertanyaan['id_hasil_kuisioner']); //send email

			$t_hk_graph = array();
			$pertanyaan = $this->input->post('pertanyaan');
			$jenis_pertanyaan = $this->input->post('jenis_pertanyaan');
			$jawaban 	= $this->input->post('jawaban');
			for ($i=0; $i < count($pertanyaan); $i++) { 
				$t_hk_graph[] = array(
					'pertanyaan' => $pertanyaan[$i],
					'jenis_pertanyaan' => $jenis_pertanyaan[$i],
					'jawaban' => $jawaban[$i],
					'tgl_pengisian' => date('Y-m-d'),
					'id_user' => set_value('id_user'),
					'cell_production' => set_value('cell_production')
				);
			}
			$this->Admin_model->addtodb_quiz($t_hk_graph);
	
			redirect('u/hk'); //mengalihkan halaman
		}
	}
	
	// VIEW PROFILE
	public function index_profile($id_user) {
		$data = array(
			'titile'	=> 'Panel User',
			'head_menu'	=> 'Profile',
			'user'		=> $this->User_model->find_user($id_user),
        );
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/profile');
		$this->load->view('page/footer');
	}

	// UPDATE PROFILE
	public function update_master_user($id_user) {
		$this->form_validation->set_rules('nama_user','Nama User','required');
		
		if($this->form_validation->run() == FALSE){
			$data = array(
				'titile'	=> 'Panel User',
				'head_menu'	=> 'Profile',
				'user'		=> $this->User_model->find_user($id_user),
			);
			
			$this->load->view('page/header', $data);
			$this->load->view('sysadmin/profile');
			$this->load->view('page/footer');
		}else{
			$data_user = array (
				'nama_user'	=> set_value('nama_user'),
				'jk'		=> set_value('jk'),
				'email'		=> set_value('email'),
				'username'	=> set_value('username'),
				'password'	=> set_value('password'),
				'level'		=> set_value('level'),
			);
			$this->session->set_flashdata('update','Anda berhasil meng-update profile');
			$this->User_model->update_user($id_user, $data_user);
			redirect('u');
		}	
	}
	
	// HASIL KUISIONER
	public function hasil_kuisioner() {
		$data = array(
			'titile'			=> 'Panel User',
			'head_menu'			=> 'Hasil Kuisioner',
			'hasil_kuisioner'	=> $this->Admin_model->get_data_hasil_kuisioner($this->session->userdata('id_user')),
		);
		
		$this->load->view('page/header', $data);
		$this->load->view('user/hasil_kuisioner', $data);
		$this->load->view('page/footer');
	}
	
	// DETAIL HASIL KUISIONER
	public function detail_hasil_kuisioner($id_hasil_kuisioner) {
		$data = array(
			'titile'			=> 'Panel User',
			'head_menu'			=> 'Hasil Kuisioner',
			'hasil_kuisioner'	=> $this->Admin_model->get_data_detail_hasil_kuisioner($id_hasil_kuisioner),
		);
		
		$this->load->view('page/header', $data);
		$this->load->view('user/detail_hasil_kuisioner', $data);
		$this->load->view('page/footer');
	}
	
	public function email_sender($id_kuis) { 
        $hasil_kuisioner	= $this->Admin_model->get_data_detail_hasil_kuisioner($id_kuis);
        $user_mail      	= $this->User_model->get_data_email_user();
        
            $sender = "emailpengirim@gmail.com";
            $subject = "Hasil Kuisioner <noreply>";
            $message = "";

            foreach($hasil_kuisioner as $hasil_kuisioners):
                $message .= "<div>
                    <label>ID User: ".$hasil_kuisioners->id_user."</label>
                </div>";
                $encode_data_pertanyaan = $hasil_kuisioners->pertanyaan; 
                $encode_data_jawaban    = $hasil_kuisioners->jawaban; 
                $pertanyaan             = json_decode($encode_data_pertanyaan,true);
                $jawaban                = json_decode($encode_data_jawaban,true);

                for($i=0; $i<count($pertanyaan); $i++){
                    for($i=0; $i<count($jawaban); $i++){
                        $message .= "<div>
                            <label>".($i+1).". ".nl2br($pertanyaan[$i]).": </label>
                            <label> ".$jawaban[$i]."</label>
                        </div>";
                    }
                }
            endforeach;

            foreach($user_mail as $user_mails):
                $from_email = $sender;
                $to_email = "$user_mails->email";
                //Load email library
                $this->load->library('email');
                $this->email->from($from_email);
                $this->email->to($to_email);
                $this->email->subject($subject);
                $this->email->message($message);
        
                // Tampilkan pesan sukses atau error
                if ($this->email->send()) {
                    echo "berhasil $to_email <br>";
                } else {
                    echo "gagal $to_email <br>";
                }
            endforeach; 
            // var_dump($message);exit;
      }
}