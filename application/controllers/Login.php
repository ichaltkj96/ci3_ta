<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
	public function index() {
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
        
		if($this->form_validation->run() == FALSE){
			$this->load->view('login');
		}else{
			$this->load->model('User_model');
            $valid_user = $this->User_model->check_credential();
			
			if($valid_user == FALSE){
				$this->session->set_flashdata('error','Maaf, username atau password yang Anda masukkan salah. Silakan coba lagi');
				redirect('login');
			}else{
				$this->session->set_userdata('id_user',$valid_user->id_user);
				$this->session->set_userdata('nama_user',$valid_user->nama_user);
                $this->session->set_userdata('level',$valid_user->level);
                
				
				switch($valid_user->level){
					case 'user' :
                        $this->session->set_flashdata('success','Anda berhasil login sebagai user');
					    redirect('u'); 
                    break;
                    
					case 'admin' :
                        $this->session->set_flashdata('success','Anda berhasil login sebagai admin');
					    redirect('sa'); 
                    break;
                    
					case 'sysadmin' :
                        $this->session->set_flashdata('success','Anda berhasil login sebagai sysadmin');
					    redirect('sa'); 
                    break;
                    
					default: 
						$this->session->set_flashdata('error','Anda tidak memiliki akses login');
						redirect('login'); 
					break;
				}
			}
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());	
	}
}