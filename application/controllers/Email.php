<?php                     
   class Email extends CI_Controller { 

      function __construct() { 
         parent::__construct(); 
         
         $this->load->helper(array('url','form'));        
         $this->load->library('form_validation');   
         $this->load->model('Admin_model');    
         $this->load->model('User_model');    
      } 

      public function index() { 
        $hasil_kuisioner	= $this->Admin_model->get_data_detail_hasil_kuisioner('HK00000001');
        $user_mail      	= $this->User_model->get_data_email_user();
        
            $sender = "emailpengirim@gmail.com";
            $subject = "Hasil Kuisioner <noreply>";
            $message = "";

            foreach($hasil_kuisioner as $hasil_kuisioners):
                $message .= "<div>
                    <label>ID User: ".$hasil_kuisioners->id_user."</label>
                </div>";
                $encode_data_pertanyaan = $hasil_kuisioners->pertanyaan; 
                $encode_data_jawaban    = $hasil_kuisioners->jawaban; 
                $pertanyaan             = json_decode($encode_data_pertanyaan,true);
                $jawaban                = json_decode($encode_data_jawaban,true);

                for($i=0; $i<count($pertanyaan); $i++){
                    for($i=0; $i<count($jawaban); $i++){
                        $message .= "<div>
                            <label>".($i+1).". ".nl2br($pertanyaan[$i]).": </label>
                            <label> ".$jawaban[$i]."</label>
                        </div>";
                    }
                }
            endforeach;

            foreach($user_mail as $user_mails):
                $from_email = $sender;
                $to_email = "$user_mails->email";
                //Load email library
                $this->load->library('email');
                $this->email->from($from_email);
                $this->email->to($to_email);
                $this->email->subject($subject);
                $this->email->message($message);
        
                // Tampilkan pesan sukses atau error
                if ($this->email->send()) {
                    echo "berhasil $to_email <br>";
                } else {
                    echo "gagal $to_email <br>";
                }
            endforeach; 
            // var_dump($message);exit;
      }
} 