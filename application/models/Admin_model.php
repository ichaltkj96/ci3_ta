<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	
	// MASTER KUISIONER
    public function get_data_master_kuisioner() {
        $hasil = $this->db
        ->select('*')
        ->from('t_kuisioner')
		->order_by('id_kuisioner', 'ASC')
        ->get();
        if($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return false;
        }
	}
	
	// GET LAST ID_KUISIONER
	public function get_kuisioner_id() {
		$hasil=$this->db->query('SELECT MAX(RIGHT(`id_kuisioner`,6)) AS kd_max FROM `t_kuisioner`');
		$kode = "";
		if($hasil->num_rows() > 0){
			foreach($hasil->result() as $kd){
                $tmp = ((int)$kd->kd_max)+1;
                $kode = sprintf("%06s", $tmp);
            }
		}else{
			$kode = "000001";
		}

		$karakter = "QUIZ";
		return $karakter.$kode;
	}
	
	// INSERT DATA KUISIONER
	public function insert_kuisioner($data_kuisioner) {
		$this->db->insert('t_kuisioner',$data_kuisioner);
	}

	// FIND DATA UPDATE KUISIONER
	public function find_kuisioner($id_kuisioner) {
		$hasil = $this->db->where('id_kuisioner',$id_kuisioner)
		->limit(1)
		->get('t_kuisioner');
		if($hasil->num_rows() > 0){
			return $hasil->row();
		}else{
			return array();
		}
    }

	// UPDATE DATA KUISIONER
	public function update_kuisioner($id_kuisioner, $data_kuisioner) {
		$this->db->where('id_kuisioner',$id_kuisioner)
		->update('t_kuisioner',$data_kuisioner);	
	}
	
	// GET LAST ID_HASIL_KUISIONER
	public function get_hasil_kuisioner_id() {
		$hasil=$this->db->query('SELECT MAX(RIGHT(`id_hasil_kuisioner`,8)) AS kd_max FROM `t_hasil_kuisioner`');
		$kode = "";
		if($hasil->num_rows() > 0){
			foreach($hasil->result() as $kd){
                $tmp = ((int)$kd->kd_max)+1;
                $kode = sprintf("%08s", $tmp);
            }
		}else{
			$kode = "00000001";
		}

		$karakter = "HK";
		return $karakter.$kode;
	}

	// INSERT HASIL KUISIONER
    public function insert_hasil_kuisioner($data_pertanyaan){
        $result=$this->db->insert('t_hasil_kuisioner',$data_pertanyaan);
        return $result;
	}

	// DATA HASIL KUISIONER PER-USER
    public function get_data_hasil_kuisioner($id_user){
		$hasil = $this->db
        ->select('*')
		->from('t_hasil_kuisioner')
		->where('id_user',$id_user)
		->order_by('tgl_pengisian', 'DESC')
        ->get();
        if($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return false;
		}
	}

	// DETAIL DATA HASIL KUISIONER PER-USER
    public function get_data_detail_hasil_kuisioner($id_hasil_kuisioner){
        $hasil = $this->db
		->limit(1)
        ->select('*')
		->from('t_hasil_kuisioner')
		->where('id_hasil_kuisioner',$id_hasil_kuisioner)
        ->get();
        if($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return false;
		}
	}

	// ALL DATA HASIL KUISIONER
    public function get_all_hasil_kuisioner(){
		$hasil = $this->db
        ->select('a.*, b.nama_user')
		->from('t_hasil_kuisioner as a')
		->join('t_user as b','a.id_user=b.id_user','INNER')
		->order_by('a.tgl_pengisian', 'DESC')
        ->get();
        if($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return false;
		}
	}

	// ALL DATA PICTURE
    public function get_data_picture($date_select){
		if($date_select){
			$hasil = $this->db
			->select('file')
			->from('t_hasil_kuisioner')
			->where('tgl_pengisian',$date_select)
			->order_by('id_hasil_kuisioner', 'DESC')
			->get();
		}else{
			$hasil = $this->db
			->select('file')
			->from('t_hasil_kuisioner')
			->order_by('id_hasil_kuisioner', 'DESC')
			->get();
		}
        if($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return false;
		}
	}

	// DETAIL ALL DATA HASIL KUISIONER
    public function get_all_detail_hasil_kuisioner($id_hasil_kuisioner){
        $hasil = $this->db
		->limit(1)
        ->select('a.*, b.nama_user')
		->from('t_hasil_kuisioner as a')
		->join('t_user as b','a.id_user=b.id_user','INNER')
		->where('a.id_hasil_kuisioner',$id_hasil_kuisioner)
        ->get();
        if($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return false;
		}
	}

	// INSERT HASIL KUISIONER
	public function addtodb_quiz($t_hk_graph){
		$result = $this->db->insert_batch('t_hk_graph', $t_hk_graph);
        return $result;
	}
	
	// GET DATA LABEL CHART
    public function get_data_label_chart($date_select) {
		if($date_select != ""){
			$hasil = $this->db
				->distinct()
				->select('cell_production')
				->from('t_hk_graph')
				->where('jenis_pertanyaan!=""')
				->where('tgl_pengisian',$date_select)
				->order_by('cell_production', 'ASC')
				->get();
		}else{
			$hasil = $this->db
				->distinct()
				->select('cell_production')
				->from('t_hk_graph')
				->where('jenis_pertanyaan!=""')
				->order_by('cell_production', 'ASC')
				->get();
		}

		if($hasil->num_rows() > 0) {
			return $hasil->result();
		} else {
			return false;
		}
	}
	
}