<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	
	// LOGIN USER
	public function check_credential()
	{
		$username = set_value('username');
		$password = set_value('password');
		$hasil = $this->db->where('username',$username)
		->where('password',$password)
		->limit(1)
		->get('t_user');
		
		if($hasil->num_rows() > 0){
			return $hasil->row();
		}else{
			return array();
		}
	}
	
	// MASTER USER
    public function get_data_master_user() {
        $hasil = $this->db
        ->select('*')
        ->from('t_user')
		->order_by('id_user', 'ASC')
        ->get();
        if($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return false;
        }
	}
	
	// DATA USER SEND EMAIL
    public function get_data_email_user() {
        $hasil = $this->db
        ->select('email')
        ->from('t_user')
        ->where('send_email', 1)
        ->get();
        if($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return false;
        }
	}
	
	// GET LAST ID_USER
	public function get_user_id() {
		$hasil=$this->db->query('SELECT MAX(RIGHT(`id_user`,7)) AS kd_max FROM `t_user`');
		$kode = "";
		if($hasil->num_rows() > 0){
			foreach($hasil->result() as $kd){
                $tmp = ((int)$kd->kd_max)+1;
                $kode = sprintf("%07s", $tmp);
            }
		}else{
			$kode = "0000001";
		}

		$karakter = "USR";
		return $karakter.$kode;
	}
	
	// INSERT DATA USER
	public function insert_user($data_user) {
		$this->db->insert('t_user',$data_user);
	}

	// FIND DATA UPDATE USER
	public function find_user($id_user) {
		$hasil = $this->db->where('id_user',$id_user)
		->limit(1)
		->get('t_user');
		if($hasil->num_rows() > 0){
			return $hasil->row();
		}else{
			return array();
		}
    }

	// UPDATE DATA USER
	public function update_user($id_user, $data_user) {
		$this->db->where('id_user',$id_user)
		->update('t_user',$data_user);	
	}
	
	// DELETE
	public function delete($where, $table) {
		$this->db->where($where);
		$this->db->delete($table);
	}
}