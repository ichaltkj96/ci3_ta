        <?php $page = $_SERVER['PHP_SELF']; $sec = "10"; ?>
        <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"><?php echo $head_menu; ?></h1>
            
            <div align="right">
                <a class="btn btn-primary" href="<?php echo site_url(); ?>sa"> <span>All Chart</span> </a>
                <a class="btn btn-outline-primary" href="<?php echo site_url(); ?>Sysadmin/index_page1" role="button">1</a>
                <a class="btn btn-primary" href="<?php echo site_url(); ?>Sysadmin/index_page2" role="button">2</a>
                <a class="btn btn-primary" href="<?php echo site_url(); ?>Sysadmin/index_page3" role="button">3</a>
            </div>
          </div>
          
          <!-- VALUE DASHBOARD -->
          <div id="reportrange" class="selectbox "  style="border: 1px solid #ddd; border-radius: 4px; padding:5px; background: #fff; cursor: pointer; margin: 0  0 4px 0;overflow: hidden; white-space: nowrap;">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"> <?php echo $date_select; ?> </i> &nbsp;
              <input type="date" class="form-control" onchange="changeSession()" name="selectdaterange1" id="selectdaterange1"/>
          </div>
          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-6 col-lg-7">
              <!-- Bar Chart -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <a target="_blank" class="btn btn-outline-primary" href="<?php echo site_url(); ?>Sysadmin/view_chart/Proses_Cutting">
                    <h6 class="m-0 font-weight-bold text-primary">Chart Kuisioner Proses Cutting</h6>
                  </a>
                </div>
                <div class="card-body">
                  <div class="chart-bar">
                    <canvas id="Proses Cutting"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-6 col-lg-7">
              <!-- Bar Chart -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <a target="_blank" class="btn btn-outline-primary" href="<?php echo site_url(); ?>Sysadmin/view_chart/Proses_NO-Sew">
                    <h6 class="m-0 font-weight-bold text-primary">Chart Kuisioner Proses NO-Sew</h6>
                  </a>
                </div>
                <div class="card-body">
                  <div class="chart-bar">
                    <canvas id="Proses NO-Sew"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-6 col-lg-7">
              <!-- Bar Chart -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <a target="_blank" class="btn btn-outline-primary" href="<?php echo site_url(); ?>Sysadmin/view_chart/Proses_Punching">
                    <h6 class="m-0 font-weight-bold text-primary">Chart Kuisioner Proses Punching</h6>
                  </a>
                </div>
                <div class="card-body">
                  <div class="chart-bar">
                    <canvas id="Proses Punching"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-6 col-lg-7">
              <!-- Bar Chart -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <a target="_blank" class="btn btn-outline-primary" href="<?php echo site_url(); ?>Sysadmin/view_chart/Proses_Printing">
                    <h6 class="m-0 font-weight-bold text-primary">Chart Kuisioner Proses Printing</h6>
                  </a>
                </div>
                <div class="card-body">
                  <div class="chart-bar">
                    <canvas id="Proses Printing"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->