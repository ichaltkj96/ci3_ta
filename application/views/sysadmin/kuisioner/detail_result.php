        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?php echo $head_menu; ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <?php foreach($hasil_kuisioner as $hasil_kuisioners):?>
                    <b> Nama User: <?= $hasil_kuisioners->nama_user; ?> </b>
                <?php endforeach; ?>
            </div>
            <div class="card-body">
                <div class="form-group" align="left">
                    <a href="<?php echo site_url(); ?>sa/hk" class="btn btn-primary">Kembali</a>
                </div><br>

                <?php foreach($hasil_kuisioner as $hasil_kuisioners):
                    $encode_data_pertanyaan = $hasil_kuisioners->pertanyaan; 
                    $encode_data_jawaban    = $hasil_kuisioners->jawaban; 
                    $pertanyaan             = json_decode($encode_data_pertanyaan,true);
                    $jawaban                = json_decode($encode_data_jawaban,true);

                    for($i=0; $i<count($pertanyaan); $i++){
                        for($i=0; $i<count($jawaban); $i++){
                            echo"<div class=\"form-group\">
                                <label class=\"col-sm-12 control-label\">
                                    ".($i+1).". ".nl2br($pertanyaan[$i]).": (*)
                                </label>

                                <div class=\"col-sm-12\">
                                    <input value=\"".$jawaban[$i]."\" class=\"form-control\" readonly>
                                </div>
                            </div>";
                        }
                    } ?>
                        
                    <div class="form-group">
                        <label for="file" class="col-sm-12 control-label">Proses Fallure Picture: (*)</label>
                        <div class="col-sm-12">
                            <a href="<?php echo site_url(); ?>assets/images/<?= $hasil_kuisioners->file; ?>">
                                <img src="<?php echo site_url(); ?>assets/images/<?= $hasil_kuisioners->file; ?>" alt="Proses Fallure Picture" height="100%" width="100%"> 
                            </a>
                        </div>
                    </div>

                <?php endforeach; ?>

                <div class="form-group" align="right">
                    <a href="<?php echo site_url(); ?>sa/hk" class="btn btn-primary">Kembali</a>
                </div><br>

            </div>
          </div>
                
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->