<?php
	$id_kuisioner = $kuisioner->id_kuisioner;
if($this->input->post('is_submitted')){
	$pertanyaan         = set_value('pertanyaan');
	$jenis_pertanyaan   = set_value('jenis_pertanyaan');
	$tipe_pertanyaan    = set_value('tipe_pertanyaan');
	$value_tipe         = set_value('value_tipe');
}else{
	$pertanyaan         = $kuisioner->pertanyaan;
	$jenis_pertanyaan   = $kuisioner->jenis_pertanyaan;
	$tipe_pertanyaan    = $kuisioner->tipe_pertanyaan;
	$value_tipe         = $kuisioner->value_tipe;
}
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?php echo $head_menu; ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
                
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $head_menu_form; ?></h6>
            </div>

                <div class="card-body">
                    <?=form_open_multipart('sa/uk/' . $id_kuisioner, ['class'=>'form-horizontal'])?> 
                    
                        <div class="form-group">
                        <?php $error = form_error("pertanyaan", "<p class='text-danger'>", '</p>'); ?>
                            <label for="pertanyaan" class="col-sm-2 control-label">Pertanyaan (*)</label>
                            <div class="col-sm-12">
                                <textarea type="text" class="form-control" id="pertanyaan" name="pertanyaan" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')"><?= $pertanyaan ?></textarea>
                            </div>
                        <?php echo $error; ?>
                        </div>
                    
                        <div class="form-group">
                        <?php $error = form_error("jenis_pertanyaan", "<p class='text-danger'>", '</p>'); ?>
                            <label for="jenis_pertanyaan" class="col-sm-2 control-label">Jenis Pertanyaan (*)</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="jenis_pertanyaan" name="jenis_pertanyaan" value="<?= $jenis_pertanyaan ?>">
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("tipe_pertanyaan", "<p class='text-danger'>", '</p>'); ?>
                            <label for="tipe_pertanyaan" class="col-sm-2 control-label">Tipe Pertanyaan (*)</label>
                            <div class="col-sm-12">
                                <select id="tipe_pertanyaan" name="tipe_pertanyaan" onchange="tipe_pertanyaan_change(this.value)" class="form-control" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')">
                                    <option value="">--- Pilih ---</option>
                                    <option value="text" <?= $tipe_pertanyaan=='text'?'selected':''; ?>>Inputan / Text</option>
                                    <option value="number" <?= $tipe_pertanyaan=='number'?'selected':''; ?>>Number / Nomor</option>
                                    <option value="radio" <?= $tipe_pertanyaan=='radio'?'selected':''; ?>>Radio Button</option>
                                    <option value="date" <?= $tipe_pertanyaan=='date'?'selected':''; ?>>Tanggal</option>
                                </select>
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group" <?= $tipe_pertanyaan=='radio'?'':'style="display:none;"'; ?> id="value_tipe">
                            <label for="value_tipe" class="col-sm-2 control-label">Value Tipe Pertanyaan</label>
                            <div class="col-sm-12">
                                <select name="value_tipe" class="form-control">
                                    <option value="">--- Pilih ---</option>
                                    <option value="Morning;Afternoon" <?= $value_tipe=='Morning;Afternoon'?'selected':''; ?>>Morning & Afternoon</option>
                                    <option value="Ya;Tidak" <?= $value_tipe=='Ya;Tidak'?'selected':''; ?>>Ya & Tidak</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group" align="right">
                            <a href="<?php echo site_url(); ?>sa/mk" class="btn btn-primary">Kembali</a>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>

                    </form>
                </div>
                    
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      
    <script>
        function tipe_pertanyaan_change(value) {            
            document.getElementById('value_tipe').style.display = value == 'radio' ? '' : 'none';
        }
    </script>