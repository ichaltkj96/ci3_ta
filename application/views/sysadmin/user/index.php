        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?php echo $head_menu; ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              
              <?php 
                $add    = $this->session->flashdata('add');
                $update = $this->session->flashdata('update');
                $delete = $this->session->flashdata('delete');
                
                if($add || $update || $delete){
                  echo '<div class="text-center alert alert-success">' .$add.$update.$delete. '</div>';
                }
              
                if($this->session->userdata('level') == 'sysadmin'){
              ?>
                <h6 class="m-0 font-weight-bold text-primary"><a href="<?php echo site_url(); ?>sa/au" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a></h6>
                <?php } ?>

            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Jenis Kelamin</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Email</th>
                      <th>Level</th>
                      <th>Konfirmasi Email</th>
                      <th>#</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Jenis Kelamin</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Email</th>
                      <th>Level</th>
                      <th>Konfirmasi Email</th>
                      <th>#</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    
                    <?php if($data_user == FALSE){ ?>
                        <tr>
                            <td colspan='7' align='center'>TIDAK ADA DATA</td>
                        </tr>
                    <?php }else{ ?>
                        <?php $no=1; foreach($data_user as $data_users): ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= $data_users->nama_user; ?></td>
                                <td><?= $data_users->jk; ?></td>
                                <td><?= $data_users->username; ?></td>
                                <td><?= md5($data_users->password); ?></td>
                                <td><?= $data_users->email; ?></td>
                                <td><?= $data_users->level; ?></td>
                                <td>
                                  <?php if($data_users->send_email == 1){
                                    echo"Ya";
                                  }else{
                                    echo"Tidak";
                                  }; ?>
                                </td>
                                <td>
                                    <?php if($this->session->userdata('level') == 'admin' && $data_users->level != 'sysadmin'){ ?>

                                      <a class="btn btn-info btn-circle btn-sm" href="<?php echo site_url(); ?>sa/uu/<?=$data_users->id_user?>"> <i class="fas fa-info-circle"></i> </a>
                                      <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal" data-target="#<?=$data_users->id_user;?>"> <i class="fas fa-trash"></i> </a>

                                      <!-- Delete User Modal-->
                                      <div class="modal fade" id="<?=$data_users->id_user;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Anda yakin ingin menghapus?</h5>
                                              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">Pilih "Hapus" jika anda yakin ingin menghapus data ini.</div>
                                            <div class="modal-footer">
                                              <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                              <a class="btn btn-primary" href="<?php echo site_url(); ?>sysadmin/delete_master_user/<?=$data_users->id_user;?>">Hapus</a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                    <?php }elseif($this->session->userdata('level') == 'sysadmin'){ ?>

                                      <a class="btn btn-info btn-circle btn-sm" href="<?php echo site_url(); ?>sa/uu/<?=$data_users->id_user?>"> <i class="fas fa-info-circle"></i> </a>
                                      <a class="btn btn-danger btn-circle btn-sm" href="" data-toggle="modal" data-target="#<?=$data_users->id_user;?>"> <i class="fas fa-trash"></i> </a>

                                      <!-- Delete User Modal-->
                                      <div class="modal fade" id="<?=$data_users->id_user;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Anda yakin ingin menghapus?</h5>
                                              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">Pilih "Hapus" jika anda yakin ingin menghapus data ini.</div>
                                            <div class="modal-footer">
                                              <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                              <a class="btn btn-primary" href="<?php echo site_url(); ?>sysadmin/delete_master_user/<?=$data_users->id_user;?>">Hapus</a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                    <?php } ?>
                                </td>
                            </tr>
                        <?php $no++; endforeach; ?>
                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
                
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->