      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Kuisioner <?php echo date('Y'); ?></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Anda yakin ingin keluar?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih "Logout" jika anda yakin ingin mengakhiri sesi ini.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?php echo site_url(); ?>login/logout">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo site_url(); ?>assets/admin_template/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/admin_template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo site_url(); ?>assets/admin_template/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo site_url(); ?>assets/admin_template/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo site_url(); ?>assets/admin_template/vendor/chart.js/Chart.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/admin_template/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/admin_template/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo site_url(); ?>assets/admin_template/js/demo/chart-bar-demo.js"></script>
  <script src="<?php echo site_url(); ?>assets/admin_template/js/demo/datatables-demo.js"></script>
  
  
  <?php if($head_menu == 'Dashboard') {
    $nm_label_chart = ""; 
    // Proses Cutting
    $pc_total_valueyes_chart = ""; $pc_total_valueno_chart = "";
    // Proses Cutting

    // Proses NO-Sew
    $pns_total_valueyes_chart = ""; $pns_total_valueno_chart = "";
    // Proses NO-Sew

    // Proses Punching
    $pp_total_valueyes_chart = ""; $pp_total_valueno_chart = "";
    // Proses Punching

    // Proses Printing
    $ppg_total_valueyes_chart = ""; $ppg_total_valueno_chart = "";
    // Proses Printing

    // Proses Painting
    $ppt_total_valueyes_chart = ""; $ppt_total_valueno_chart = "";
    // Proses Painting

    // Proses Stitching
    $psg_total_valueyes_chart = ""; $psg_total_valueno_chart = "";
    // Proses Stitching

    // Proses Assembly
    $pay_total_valueyes_chart = ""; $pay_total_valueno_chart = "";
    // Proses Assembly

    // CCQP
    $ccqp_total_valueyes_chart = ""; $ccqp_total_valueno_chart = "";
    // CCQP
    
    // var_dump($date_select);exit;;
    if($date_select){
      $date_select="and tgl_pengisian='$date_select'";
    }else{
      $date_select="";
    }
    
    if($data_label_chart){
      foreach($data_label_chart as $data_label_charts):
          $nm_label_chart .= "'Cell Production $data_label_charts->cell_production',";
          
          // Proses Cutting
          $pc_yes_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Ya" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Cutting" '.$date_select.'')->result();
          $pc_no_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Tidak" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Cutting" '.$date_select.'')->result();
          foreach($pc_yes_value_chart as $pc_yes_value_charts):
            $pc_total_valueyes_chart .= $pc_yes_value_charts->total.",";
          endforeach;
          foreach($pc_no_value_chart as $pc_no_value_charts):
            $pc_total_valueno_chart .= $pc_no_value_charts->total.",";
          endforeach;
          // Proses Cutting

          // Proses NO-Sew
          $pns_yes_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Ya" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses NO-Sew" '.$date_select.'')->result();
          $pns_no_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Tidak" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses NO-Sew" '.$date_select.'')->result();
          foreach($pns_yes_value_chart as $pns_yes_value_charts):
            $pns_total_valueyes_chart .= $pns_yes_value_charts->total.",";
          endforeach;
          foreach($pns_no_value_chart as $pns_no_value_charts):
            $pns_total_valueno_chart .= $pns_no_value_charts->total.",";
          endforeach;
          // Proses NO-Sew

          // Proses Punching
          $pp_yes_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Ya" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Punching" '.$date_select.'')->result();
          $pp_no_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Tidak" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Punching" '.$date_select.'')->result();
          foreach($pp_yes_value_chart as $pp_yes_value_charts):
            $pp_total_valueyes_chart .= $pp_yes_value_charts->total.",";
          endforeach;
          foreach($pp_no_value_chart as $pp_no_value_charts):
            $pp_total_valueno_chart .= $pp_no_value_charts->total.",";
          endforeach;
          // Proses Punching

          // Proses Printing
          $ppg_yes_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Ya" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Printing" '.$date_select.'')->result();
          $ppg_no_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Tidak" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Printing" '.$date_select.'')->result();
          foreach($ppg_yes_value_chart as $ppg_yes_value_charts):
            $ppg_total_valueyes_chart .= $ppg_yes_value_charts->total.",";
          endforeach;
          foreach($ppg_no_value_chart as $ppg_no_value_charts):
            $ppg_total_valueno_chart .= $ppg_no_value_charts->total.",";
          endforeach;
          // Proses Printing

          // Proses Painting
          $ppt_yes_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Ya" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Painting" '.$date_select.'')->result();
          $ppt_no_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Tidak" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Painting" '.$date_select.'')->result();
          foreach($ppt_yes_value_chart as $ppt_yes_value_charts):
            $ppt_total_valueyes_chart .= $ppt_yes_value_charts->total.",";
          endforeach;
          foreach($ppt_no_value_chart as $ppt_no_value_charts):
            $ppt_total_valueno_chart .= $ppt_no_value_charts->total.",";
          endforeach;
          // Proses Painting

          // Proses Stitching
          $psg_yes_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Ya" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Stitching" '.$date_select.'')->result();
          $psg_no_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Tidak" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Stitching" '.$date_select.'')->result();
          foreach($psg_yes_value_chart as $psg_yes_value_charts):
            $psg_total_valueyes_chart .= $psg_yes_value_charts->total.",";
          endforeach;
          foreach($psg_no_value_chart as $psg_no_value_charts):
            $psg_total_valueno_chart .= $psg_no_value_charts->total.",";
          endforeach;
          // Proses Stitching

          // Proses Assembly
          $pay_yes_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Ya" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Assembly" '.$date_select.'')->result();
          $pay_no_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Tidak" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="Proses Assembly" '.$date_select.'')->result();
          foreach($pay_yes_value_chart as $pay_yes_value_charts):
            $pay_total_valueyes_chart .= $pay_yes_value_charts->total.",";
          endforeach;
          foreach($pay_no_value_chart as $pay_no_value_charts):
            $pay_total_valueno_chart .= $pay_no_value_charts->total.",";
          endforeach;
          // Proses Assembly

          // CCQP
          $ccqp_yes_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Ya" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="CCQP" '.$date_select.'')->result();
          $ccqp_no_value_chart = $this->db->query('SELECT COUNT(`jawaban`) as total FROM `t_hk_graph` WHERE `jawaban`="Tidak" and `cell_production`="'.$data_label_charts->cell_production.'" and `jenis_pertanyaan`="CCQP" '.$date_select.'')->result();
          foreach($ccqp_yes_value_chart as $ccqp_yes_value_charts):
            $ccqp_total_valueyes_chart .= $ccqp_yes_value_charts->total.",";
          endforeach;
          foreach($ccqp_no_value_chart as $ccqp_no_value_charts):
            $ccqp_total_valueno_chart .= $ccqp_no_value_charts->total.",";
          endforeach;
          // CCQP

      endforeach; //var_dump($data_ya_chart[0]->yax);exit;
    } ?>

    <script>
      // Bar Chart
      // Proses Cutting
      var ctx_pc = document.getElementById("Proses Cutting");
      if(ctx_pc){
        var data_pc = {
            labels: [<?= $nm_label_chart ?>],
            datasets: [
                {
                    label: "Ya",
                    backgroundColor: "blue",
                    data: [<?= $pc_total_valueyes_chart ?>]
                },
                {
                    label: "Tidak",
                    backgroundColor: "red",
                    data: [<?= $pc_total_valueno_chart ?>]
                }
            ]
        };
        var myBarChart_pc = new Chart(ctx_pc, {
          type: 'bar',
          data: data_pc,
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                maxBarThickness: 25,
              }],
            },
            title: {
                display: true,
                text: 'Proses Cutting'
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
      }
      // Proses Cutting

      // Proses NO-Sew
      var ctx_pns = document.getElementById("Proses NO-Sew");
      if(ctx_pns){
        var data_pns = {
            labels: [<?= $nm_label_chart ?>],
            datasets: [
                {
                    label: "Ya",
                    backgroundColor: "blue",
                    data: [<?= $pns_total_valueyes_chart ?>]
                },
                {
                    label: "Tidak",
                    backgroundColor: "red",
                    data: [<?= $pns_total_valueno_chart ?>]
                }
            ]
        };
        var myBarChart_pns = new Chart(ctx_pns, {
          type: 'bar',
          data: data_pns,
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                maxBarThickness: 25,
              }],
            },
            title: {
                display: true,
                text: 'Proses NO-Sew'
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
      }
      // Proses NO-Sew

      // Proses Punching
      var ctx_pp = document.getElementById("Proses Punching");
      if(ctx_pp){
        var data_pp = {
            labels: [<?= $nm_label_chart ?>],
            datasets: [
                {
                    label: "Ya",
                    backgroundColor: "blue",
                    data: [<?= $pp_total_valueyes_chart ?>]
                },
                {
                    label: "Tidak",
                    backgroundColor: "red",
                    data: [<?= $pp_total_valueno_chart ?>]
                }
            ]
        };
        var myBarChart_pp = new Chart(ctx_pp, {
          type: 'bar',
          data: data_pp,
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                maxBarThickness: 25,
              }],
            },
            title: {
                display: true,
                text: 'Proses Punching'
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
      }
      // Proses Punching

      // Proses Printing
      var ctx_ppg = document.getElementById("Proses Printing");
      if(ctx_ppg){
        var data_ppg = {
            labels: [<?= $nm_label_chart ?>],
            datasets: [
                {
                    label: "Ya",
                    backgroundColor: "blue",
                    data: [<?= $ppg_total_valueyes_chart ?>]
                },
                {
                    label: "Tidak",
                    backgroundColor: "red",
                    data: [<?= $ppg_total_valueno_chart ?>]
                }
            ]
        };
        var myBarChart_ppg = new Chart(ctx_ppg, {
          type: 'bar',
          data: data_ppg,
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                maxBarThickness: 25,
              }],
            },
            title: {
                display: true,
                text: 'Proses Printing'
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
      }
      // Proses Printing

      // Proses Painting
      var ctx_ppt = document.getElementById("Proses Painting");
      if(ctx_ppt){
        var data_ppt = {
            labels: [<?= $nm_label_chart ?>],
            datasets: [
                {
                    label: "Ya",
                    backgroundColor: "blue",
                    data: [<?= $ppt_total_valueyes_chart ?>]
                },
                {
                    label: "Tidak",
                    backgroundColor: "red",
                    data: [<?= $ppt_total_valueno_chart ?>]
                }
            ]
        };
        var myBarChart_ppt = new Chart(ctx_ppt, {
          type: 'bar',
          data: data_ppt,
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                maxBarThickness: 25,
              }],
            },
            title: {
                display: true,
                text: 'Proses Painting'
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
      }
      // Proses Painting

      // Proses Stitching
      var ctx_psg = document.getElementById("Proses Stitching");
      if(ctx_psg){
        var data_psg = {
            labels: [<?= $nm_label_chart ?>],
            datasets: [
                {
                    label: "Ya",
                    backgroundColor: "blue",
                    data: [<?= $psg_total_valueyes_chart ?>]
                },
                {
                    label: "Tidak",
                    backgroundColor: "red",
                    data: [<?= $psg_total_valueno_chart ?>]
                }
            ]
        };
        var myBarChart_psg = new Chart(ctx_psg, {
          type: 'bar',
          data: data_psg,
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                maxBarThickness: 25,
              }],
            },
            title: {
                display: true,
                text: 'Proses Stitching'
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
      }
      // Proses Stitching

      // Proses Assembly
      var ctx_pay = document.getElementById("Proses Assembly");
      if(ctx_pay){
        var data_pay = {
            labels: [<?= $nm_label_chart ?>],
            datasets: [
                {
                    label: "Ya",
                    backgroundColor: "blue",
                    data: [<?= $pay_total_valueyes_chart ?>]
                },
                {
                    label: "Tidak",
                    backgroundColor: "red",
                    data: [<?= $pay_total_valueno_chart ?>]
                }
            ]
        };
        var myBarChart_pay = new Chart(ctx_pay, {
          type: 'bar',
          data: data_pay,
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                maxBarThickness: 25,
              }],
            },
            title: {
                display: true,
                text: 'Proses Assembly'
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
      }
      // Proses Assembly

      // CCQP
      var ctx_ccqp = document.getElementById("CCQP");
      if(ctx_ccqp){
        var data_ccqp = {
            labels: [<?= $nm_label_chart ?>],
            datasets: [
                {
                    label: "Ya",
                    backgroundColor: "blue",
                    data: [<?= $ccqp_total_valueyes_chart ?>]
                },
                {
                    label: "Tidak",
                    backgroundColor: "red",
                    data: [<?= $ccqp_total_valueno_chart ?>]
                }
            ]
        };
        var myBarChart_ccqp = new Chart(ctx_ccqp, {
          type: 'bar',
          data: data_ccqp,
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                maxBarThickness: 25,
              }],
            },
            title: {
                display: true,
                text: 'CCQP'
            },
            tooltips: {
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
              }
            },
          }
        });
      }
      // CCQP

      $(document).ready(function() {
        $("#selectdaterange").change(function changeSession() {
          var date_select = this.value;
          window.location = "<?php echo base_url(); ?>index.php/Sysadmin/index_bydate/" + date_select;
        }); 
      });

      $(document).ready(function() {
        $("#selectdaterange1").change(function changeSession() {
          var date_select = this.value;
          window.location = "<?php echo base_url(); ?>index.php/Sysadmin/index_bydate1/" + date_select;
        }); 
      });

      $(document).ready(function() {
        $("#selectdaterange2").change(function changeSession() {
          var date_select = this.value;
          window.location = "<?php echo base_url(); ?>index.php/Sysadmin/index_bydate2/" + date_select;
        }); 
      });

      $(document).ready(function() {
        $("#selectdaterangev").change(function changeSession() {
          var date_select = this.value;
          var date_views = document.getElementById("date_views").value;
          window.location = "<?php echo base_url(); ?>index.php/Sysadmin/index_bydatev/"+date_views+"/"+date_select;
        }); 
      });
    </script>
    
  <?php } ?>

</body>

</html>