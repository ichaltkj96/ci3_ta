        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?php echo $head_menu; ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3"></div>
            <div class="card-body">
                    
                <?php if($data_kuisioner == FALSE){ ?>
                    <font size='7' align='center'>TIDAK ADA KUISIONER</font>
                <?php }else{ ?>
                    <?=form_open_multipart('u',['class'=>'form-horizontal'])?>

                        <input type="hidden" class="form-control" name="id_user" value="<?php echo $this->session->userdata('id_user'); ?>">
                        <input type="hidden" class="form-control" name="cell_production" id="cell_production">
                        <?php $no=0; foreach($data_kuisioner as $data_kuisioners): ?>
                            <div class="form-group">

                                <label class="col-sm-12 control-label">
                                    <input type="hidden" class="form-control" name="pertanyaan[]" value="<?= $data_kuisioners->pertanyaan; ?>">
                                    <input type="hidden" class="form-control" name="jenis_pertanyaan[]" value="<?= $data_kuisioners->jenis_pertanyaan; ?>">
                                    <?= $no+1; ?>. <?= nl2br($data_kuisioners->pertanyaan); ?>: (*)
                                </label>

                                <div class="col-sm-12">
                                    <?php $vp_explode = explode(";", $data_kuisioners->value_tipe);
                                    if(count($vp_explode)>1){
                                        for($nvp=0; $nvp<count($vp_explode); $nvp++){
                                            if($data_kuisioners->tipe_pertanyaan =='radio'){
                                                echo "<input value=\"".$vp_explode[$nvp]."\" type=\"$data_kuisioners->tipe_pertanyaan\" name=\"jawaban[$no]\" autocomplete=\"off\" required oninvalid=\"this.setCustomValidity('Field ini tidak boleh kosong')\" oninput=\"setCustomValidity('')\">
                                                <label for=\"sysadmin\">".$vp_explode[$nvp]."</label><br>";
                                            }elseif($vp_explode[$nvp]){
                                                echo "<input value=\"".$vp_explode[$nvp]."\" type=\"$data_kuisioners->tipe_pertanyaan\" name=\"jawaban[]\" autocomplete=\"off\" required oninvalid=\"this.setCustomValidity('Field ini tidak boleh kosong')\" oninput=\"setCustomValidity('')\">
                                                <label for=\"sysadmin\">".$vp_explode[$nvp]."</label><br>";
                                            }
                                        }
                                    }else{
                                        if($data_kuisioners->tipe_pertanyaan == 'number'){ 
                                            if($data_kuisioners->pertanyaan == 'Cell Production'){ ?>
                                                <input type="<?= $data_kuisioners->tipe_pertanyaan; ?>" min="1" max="100" class="form-control" name="jawaban[]" autocomplete="off" required oninvalid="this.setCustomValidity('Field ini harus angka')" oninput="setCustomValidity('')"  onchange="value_cell_production(this.value)">
                                            <?php }else{ ?>
                                                <input type="<?= $data_kuisioners->tipe_pertanyaan; ?>" min="1" max="100" class="form-control" name="jawaban[]" autocomplete="off" required oninvalid="this.setCustomValidity('Field ini harus angka')" oninput="setCustomValidity('')">
                                            <?php } ?>
                                        <?php }else{ ?>
                                            <input type="<?= $data_kuisioners->tipe_pertanyaan; ?>" class="form-control" name="jawaban[]" autocomplete="off" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')">
                                        <?php } ?>
                                    <?php } ?>
                                </div>

                            </div>
                        <?php $no++; endforeach; ?>
                        
                        <div class="form-group">
                            <label for="file" class="col-sm-12 control-label">Proses Fallure Picture: (*)</label>
                            <div class="col-sm-12">
                                <input type="file" class="form-control" id="userfile" name="userfile" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')">
                                <font size="2" color="blue"><b>Format File: gif, jpg, png, jpeg</b></font>
                            </div>
                        </div>
                        
                        <div class="form-group" align="right">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                <?php } ?>

            </div>
          </div>
                
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    <script>
        function value_cell_production(val) {
            document.getElementById("cell_production").value=val;
        }
    </script>