<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['mailtype'] ='html';
$config['charset'] ='utf-8';

$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.mailgun.org';
$config['smtp_port'] = 587;
$config['smtp_user'] = '';
$config['smtp_pass'] = '';
$config['smp_timeout'] = '4';
$config['crlf'] = '\n';
$config['newline'] = '\r\n';