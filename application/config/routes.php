<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// CTRL SYSADMIN
$route['sa'] = 'Sysadmin';

// PROFILE SYSADMIN
$route['sa/p/(:any)'] = 'Sysadmin/index_profile/$1';

// MASTER CRUD USER
$route['sa/mu']         = 'Sysadmin/index_master_user'; // VIEW
$route['sa/au']         = 'Sysadmin/add_master_user'; // ADD
$route['sa/uu/(:any)']  = 'Sysadmin/update_master_user/$1'; // UPDATE

// MASTER CRUD KUISIONER
$route['sa/mk']         = 'Sysadmin/index_master_kuisioner'; // VIEW
$route['sa/ak']         = 'Sysadmin/add_master_kuisioner'; // ADD
$route['sa/uk/(:any)']  = 'Sysadmin/update_master_kuisioner/$1'; // UPDATE
$route['sa/hk']         = 'Sysadmin/hasil_kuisioner'; // HASIL KUISIONER
$route['sa/dhk/(:any)'] = 'Sysadmin/detail_hasil_kuisioner/$1'; // DETAIL HASIL KUISIONER

// VIEW KUISIONER USER
$route['u']             = 'User'; // VIEW //ADD
$route['u/p/(:any)']    = 'User/index_profile/$1'; // PROFILE
$route['u/uu/(:any)']   = 'User/update_master_user/$1'; // UPDATE PROFILE
$route['u/hk']          = 'User/hasil_kuisioner'; // HASIL KUISIONER
$route['u/dhk/(:any)']  = 'User/detail_hasil_kuisioner/$1'; // DETAIL HASIL KUISIONER